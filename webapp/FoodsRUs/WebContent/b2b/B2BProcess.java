import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * A Java Program that goes through the directory,
 * and looks for all_orders.xml file, and gets all the 
 * orders purchased today, and then for each of the items
 * makes a WSDL connection to server and computes the cheapest
 * value from Vancouver, Toronto, Montreal.
 * 
 * Finally Generates an HTML Report with all items combined.
 * 
 * @author Abdullah Rubiyath <cse03256>
 * @author Federico Yan Yi <cse>
 *
 */

public class B2BProcess {
		
	private static String basePath;	
	
	/* declare two constant variables: .xml extension and the all_orders */
	private static final String FILE_EXT_XML = ".xml";
	private static final String FILE_EXT_HTML = ".html";	

	private static final String ORDER_KEY = "cse73069p5";
	private static String todayDate; 
	private static final String TEMPLATE_HTML = "PTemplate.html";
	
	
	public static final String ORDER_FILE = "all_orders" + FILE_EXT_XML;
	public static final String FILE_PREFIX = "po";
	public static final String FILE_SEPARATOR = "_";
	public static final String DEFAULT_FOLDER = "../res/order/";
	private final String[] soapUrls = {			
		"http://red.cse.yorku.ca:4413/axis/YYZ.jws?wsdl",
		"http://red.cse.yorku.ca:4413/axis/YHZ.jws?wsdl",
		"http://red.cse.yorku.ca:4413/axis/YVR.jws?wsdl"
	};
		
	private final String[] providerNames = { 
		"YYZ (Toronto)", "YHZ (Halifax)", "YVR (Vancouver)"
	};	


	/* A private class to keep track of providers for each item */
	private class Provider {
		String name;
		String itemNumber;
		int quantity;
		double price;
		int index;
		String status;
		
		@Override
		public String toString() {
			return "[ name: " + name + ", item#: " + itemNumber + ", qty: " + 
					  quantity + ", price: " + price + "]";
		}
	}

	
	/**
	 * A Private class for storing all the error messages
	 * specific to this class.
	 * 
	 */
	private class Errors {
		public static final String FILE_NOT_FOUND = 
				ORDER_FILE + " not found. Please ensure this file is Present!";
		
		public static final String NO_ORDERS = 
				"There are no orders to process!";
	}
	
	/**
	 * Constructs a Folder for B2BProcess to look for order
	 * 
	 * @param processDate
	 * @throws ParseException 
	 */
	public B2BProcess(String processDate) throws ParseException 
	{
		basePath = DEFAULT_FOLDER;
		Date date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(processDate);
		this.setDate(date);
	}
	
	/**
	 * if no param is provided, use default structure
	 * (as relative)
	 */
	public B2BProcess() {
		basePath = DEFAULT_FOLDER;
		this.setDate(new Date());
	}
	
	private void setDate(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		todayDate = sdf.format(d);		
	}
	
	/**
	 * Starts the Process of procurement
	 */
	public void start() {
		
		List<String> xmlFiles = this.getOrderFiles();
		
		if (xmlFiles.size() <= 0) {
			System.out.println(Errors.NO_ORDERS);
			return;
		}
		
		/*
		 * at this point, we have the list of files!
		 */
		
		// now go through all the files and put the items in a HashMap
		HashMap<String, Integer> itemsMap = new HashMap<String, Integer>();
		for (int i = 0; i < xmlFiles.size(); i++) {
			// go through all the files, and add each Element in the HashMap
			this.putItems(basePath + xmlFiles.get(i), itemsMap);
		}
		
		// at this point, I have accumulated ALL the orders of today with their quantity
		// now its time to go for WSDL with Toronto, Vancouver, Montreal?
		Provider[] providers = new Provider[itemsMap.size()];
		int index = 0;
		
		//System.out.println("Total Items to Process: " + itemsMap.size());
		
		try {
			
			System.out.printf("Processing %d distinct Items..%n", itemsMap.size());
			for (String itemNumber: itemsMap.keySet()) 
			{
	//			System.out.print("Item#:" + itemNumber + 
	//							   ", qty:" + itemsMap.get(itemNumber));	
				
				providers[index] = requestMinPrice(itemNumber);
				
				// there must be a valid provider
				assert (providers[index] != null);				
				providers[index].quantity = itemsMap.get(itemNumber);				
		//		System.out.println(", Provider: [" + providers[index].name + 
		//						   ", $" + providers[index].price +"]");				
				index++;
			}
			
			// place the orders for each of the provider
			System.out.println("Placing Orders ... ");
			int totalOrders = this.placeOrders(providers);
			
			System.out.println("Generating Report...");
			/* now generate a HTML Report File */
			this.generateReport(providers, totalOrders);		
			
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Please Try Again!");
			System.exit(1);
		}
				
	}
	
	
	/**
	 * 
	 * @param list The List of Providers
	 * @throws FileNotFoundException 
	 */
	private void generateReport(Provider[] list, int total) throws Exception {		
		assert(list != null);		
		File file = new File(TEMPLATE_HTML);
		Scanner input = new Scanner(file);
		StringBuffer bfr = new StringBuffer();
		
		while (input.hasNext()) {
			bfr.append(input.nextLine());
		}
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyyy-mm-dd hh:mm:ss"); 

		/* now, replace the texts and write to another file */
		String htmlOutput = bfr.toString().replace("{submitted}", todayDate);
		htmlOutput = htmlOutput.replace("{generated}",  dt.format(new Date()));
		htmlOutput = htmlOutput.replace("{total}", total + "");
		
		// now put all the items in the HTML Table!
		StringBuffer itemsBuffer = new StringBuffer();
		double totalCost = 0;
		for (int i=0; i < list.length; i++) {
			itemsBuffer.append("<tr>")
					   .append("<td>" + list[i].itemNumber + "</td>")
					   .append("<td>" + list[i].name + "</td>")
					   .append("<td>" + list[i].quantity + "</td>")
					   .append("<td class='price'>" + list[i].price + "</td>")					   
					   .append("<td>" + list[i].status + "</td>")
					   .append("</tr>\n");
			
			totalCost += list[i].quantity * list[i].price;
		}
		
		
		htmlOutput = htmlOutput.replace("{orders}", itemsBuffer.toString());
		htmlOutput = htmlOutput.replace("{totalcost}", totalCost + "");
		
		// now its time to write a File!
		
		File outputFile = new File(basePath + todayDate + FILE_EXT_HTML);
		
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		}
		
		// file must exist, as this point!
		assert (outputFile.exists());
		
		// now, write to file and close!
		PrintWriter pw = new PrintWriter(outputFile);
		pw.println(htmlOutput);
		pw.close();
		
		System.out.println("Report written at: " + outputFile.getCanonicalPath());
				
	}
	
	
	/**
	 * Places an Order and Generates an HTML File for the day?
	 * and returns the total orders that have been placed today.
	 * @param providerList
	 */
	private int placeOrders(Provider[] providerList) throws Exception {
		
		assert (providerList != null);
		int total = 0;
		
		/**
		 * Go through all the providers Selected and place Orders accordingly!
		 */
		String tns, number, quantity;
		
		for (int i=0; i < providerList.length; i++) {

			tns = soapUrls[providerList[i].index];
			number = providerList[i].itemNumber;
			quantity = providerList[i].quantity + "";
			
			SOAPMessage msg = MessageFactory.newInstance().createMessage();
			
			MimeHeaders header = msg.getMimeHeaders();
			header.addHeader("SOAPAction", "");
			
			SOAPPart soap = msg.getSOAPPart();
			SOAPEnvelope envelope = soap.getEnvelope();
			SOAPBody body = envelope.getBody();
			
			SOAPElement elem = body.addChildElement("order");
			elem.addChildElement("itemNumber").addTextNode(number);
			elem.addChildElement("quantity").addTextNode(quantity);
			elem.addChildElement("key").addTextNode(ORDER_KEY);
					
//			body.addChildElement("quote").addChildElement("itemNumber").addTextNode(args[1]);
			//body.addChildElement("root").addChildElement("x").addTextNode(args[0]);

			//msg.writeTo(System.out);
			
			SOAPConnection sc = SOAPConnectionFactory.newInstance().createConnection();
			SOAPMessage resp = sc.call(msg, new URL(tns));
			sc.close();
			
			//msg.writeTo(System.out);
			//resp.writeTo(System.out);

			org.w3c.dom.Node node = resp.getSOAPPart().getEnvelope().getBody()
									    .getElementsByTagName("orderReturn").item(0);
			
			//System.out.println("\nThe answer is");
			//System.out.println(node.getTextContent());	
			providerList[i].status = node.getTextContent();
			total += providerList[i].quantity;
		}
		
		return total;
	}
	
	
	/**
	 * Gets the minimum price of an item and returns the Provider with 
	 * the minimal price.
	 * 
	 * @return
	 */
	private Provider requestMinPrice(String itemNumber) throws Exception {
		
		/* set the provider's itemNumber initially */
		Provider selectedProvider = new Provider();
		selectedProvider.itemNumber = itemNumber;
		
		// put a significantly large value initially		
		double lowestPrice = Integer.MAX_VALUE;
		
		for (int i = 0; i < soapUrls.length; i++) {
			
			String tns = soapUrls[i];
			
			SOAPMessage msg = MessageFactory.newInstance().createMessage();
			
			MimeHeaders header = msg.getMimeHeaders();
			header.addHeader("SOAPAction", "");
			
			SOAPPart soap = msg.getSOAPPart();
			SOAPEnvelope envelope = soap.getEnvelope();
			SOAPBody body = envelope.getBody();
			
	//		System.out.println(soap.toString() + " <> " + body.toString());
			
			body.addChildElement("quote").addChildElement("itemNumber").addTextNode(itemNumber);
			
			SOAPConnection sc = SOAPConnectionFactory.newInstance().createConnection();
			SOAPMessage resp = sc.call(msg, new URL(tns));
			sc.close();
			
	//		msg.writeTo(System.out);
	//		resp.writeTo(System.out);
	
			org.w3c.dom.Node node = resp.getSOAPPart().getEnvelope()
										.getBody().getElementsByTagName("quoteReturn").item(0);
			
			Double price = Double.parseDouble(node.getTextContent().trim());
			
			
			if (price < lowestPrice && price >= 0) {
				selectedProvider.price = price;
				selectedProvider.name = providerNames[i];
				selectedProvider.index = i;
				
				assert(selectedProvider.price < lowestPrice && selectedProvider.price >= 0);				
				lowestPrice = price;
			}
			
		}		
		
		// it is guaranteed that at least one of the provider has a price >= 0
		assert (selectedProvider.price >= 0);
		
		return selectedProvider;
	}
	
	
	
	
	/**
	 * It retrieves all the items (item) in the file specified by fileName,
	 * and puts them in the HashMap
	 * 
	 * @param poFile		The PO File which contains the purchase history
	 * @param itemsMap		An ItemsMap where item with its quantity is to be put
	 */
	
	private void putItems(String poFile, HashMap<String, Integer> itemsMap) {
		try {
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true); 
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(poFile);
	
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
	
			// get all the orders that was made today, along with their quantity
			XPathExpression expr = xpath.compile("//item");
			XPathExpression qtyExpr = xpath.compile("//item/quantity");
			
			/* make 2 separate queries -> one for item, and another for quantity */
			NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			NodeList qtyList = (NodeList) qtyExpr.evaluate(doc, XPathConstants.NODESET);
			
			// the two lengths has to be equal!
			assert (nodeList.getLength() == qtyList.getLength());
			
			String number, qty;
			int itemQty, current;
			
			System.out.println("File: " + poFile + ", items: " + nodeList.getLength());
			
			/* put all the items number and its quantity in a HashMap */
			for (int i = 0; i < nodeList.getLength(); i++) {
				// get number, and its quantitiy
				number = nodeList.item(i).getAttributes().getNamedItem("number").getNodeValue();
				qty = qtyList.item(i).getFirstChild().getNodeValue();
				itemQty = Integer.parseInt(qty);
				
				// check if number is already present in Map or not
				if (itemsMap.get(number) == null) {					
					// no number is present on Map, so, put it
					itemsMap.put(number, itemQty);
				}
				else {
					// item already present, update its quantity
					current = itemsMap.get(number);
					itemsMap.put(number, current + itemQty);
				}
			}
		
		}
		catch(Exception e) {			
			e.printStackTrace();
			System.out.println("Please Try Again!");
			System.exit(1);
		}
	}
	
	
	/**
	 * Gets all the Order files that has been completed today.
	 * @return
	 */
	private List<String> getOrderFiles() 
	{
		/* store the xmlFile and today's Date */
		String xmlFile = basePath + ORDER_FILE;
		
		System.out.println("Processing all Orders matching: " + todayDate + "...");
		
		File file = new File(xmlFile);
		if (!file.exists()) {
			System.out.println(Errors.FILE_NOT_FOUND);
			System.exit(1);
		}
		
		List<String> urls = new ArrayList<String>();
		
		try {
			
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true); 
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(xmlFile);

			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();

			// get all the orders that was made today
			XPathExpression expr = xpath.compile("//order[@submitted='" + todayDate + "']");			
			NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			
			System.out.println("Total Orders found today: " + nodeList.getLength());
			
			/* add all the urls in the list now */
			for (int i = 0; i < nodeList.getLength(); i++) 
			{
				String account = nodeList.item(i).getAttributes().getNamedItem("customer").getNodeValue();
				String orderId = nodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
				
				urls.add(FILE_PREFIX +  account + FILE_SEPARATOR + orderId + FILE_EXT_XML);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Please Try Again!");
			System.exit(1);
		}
	    
		return urls;
	}
	
	

	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		B2BProcess b2b;
		
		if (args.length < 1) {
			// use current directory
			System.out.println("Looking in " + B2BProcess.DEFAULT_FOLDER + B2BProcess.ORDER_FILE);
			b2b = new B2BProcess();
		}
		else {
			System.out.println("Processing date: " + args[0] + "...");
			b2b = new B2BProcess(args[0]);			
		}
		
		b2b.start();
	}

}
