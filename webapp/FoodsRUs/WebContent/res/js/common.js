/* 
 * Some Common JS functionality. 
 * Functionalities available:
 * - Autosuggest (SearchComponent)
 * - FormValidation (formComponent)
 * 
 * @author Abdullah Rubiyath
 * 
 */

(function() {

	/**
	 * local variable searchComponent is used
	 * 
	 * to manage autosuggest feature of search results.
	 * The Component is structured in MVC 
	 * 
	 * model - gets the data from Server asynchronously
	 * View  - Generates the View 
	 * Controller - handles the events triggered by users
	 * 
	 */
	var searchComponent = {
		url: null,	
		ids: {
			input: "search-input",
			resultWrapper: "search-result",
			result: "search-result-links",
			hide: "hide-search-result"
		},
		events: { 
			keyup: "keyup", 
			blur: "blur",
			click: "click"
		},
		limit: 5,
		controller: null,
		model: null,
		view: null
	};
	
	/* the model for Search Component */
	searchComponent.model = function(queryData, viewFunc) {
		/* make an async query to Server and then callback the view */
		eApp.ajaxRequest(searchComponent.url, "GET", function(responseXML, responseText) {
			
			/* parse the string from Server into JSON Object */
			var jsonData = JSON.parse(responseText);
			
			/* now call the view with the JSON data from Server */
			viewFunc(true, jsonData);
			
		}, null);
	};
	
	/**
	 * Search Controller handles keyup and blur event
	 */
	searchComponent.controller = {
			
		keyup: function(event) {
			/* gets the data from model and passes it to view */
			var value = document.getElementById(searchComponent.ids.input).value;
			searchComponent.model(value, searchComponent.view);			
		},
		blur: function(event) {
			// no data to get, so simply tell the view to hide
			searchComponent.view(false);
		}
	};
	
	/* generating the view */
	searchComponent.view = function(display, serverData) 
	{
		var resultElem = document.getElementById(searchComponent.ids.result);
		var inputElem = document.getElementById(searchComponent.ids.input);
		if (display == true) {
			if (inputElem.value == '') {
				resultElem.parentNode.className = 'hide';
			}
			else {
				resultElem.parentNode.className = 'show';
				
				var resultArr = [];
				var list = serverData.items;

				var matches = 0;				
				for (var i = 0; i < list.length && matches < searchComponent.limit; i++) 
				{
					/* look for a match with the element and list[i].name */		
					if ((list[i].name.toLowerCase().indexOf(inputElem.value.toLowerCase()) != -1) || 
						(list[i].number.toLowerCase().indexOf(inputElem.value.toLowerCase()) != -1))
					{	
					/*	console.log('<li>' 
								+	'<a href="'+ eApp.contextPath + '/search?q=' 
								+		list[i].number + '" title="' + list[i].name + '">' 
								+	list[i].name 
								+	"<h4>" + list[i].name + "</h4>"
								+	'<h6>Item #:' + list[i].number + '</h6>' 
								+ '</a>'											
								+'</li>');
								*/
						resultArr.push('<li>' 
								+	'<a href="'+ eApp.contextPath + '/search?q=' 
								+		list[i].number + '" title="' + list[i].name + '">' 
								+	"<h4>" + list[i].name + "</h4>"
								+	'<h6>Item #:' + list[i].number + '</h6>' 
								+ '</a>'											
								+'</li>');
						
						matches++;		   
					}
					else {
						console.log("NO match?!!");
					}
				}
				
				if (resultArr.length < 1) {
					// display no search result found?
					resultElem.parentNode.className = 'hide';
				}
				// add a focus to the result!
				resultElem.innerHTML = resultArr.join("");
			}
		}
		else {
			resultElem.parentNode.className = 'hide';
		}
	};
	
	
	/** 
	 * @param app
	 */
	searchComponent.init = function(urlPath) 
	{
		searchComponent.url = urlPath;
		
		eApp.addEvent(document.getElementById(searchComponent.ids.input),
				 searchComponent.events.keyup, 
				 searchComponent.controller.keyup, true);
		
		//eApp.addEvent(document.getElementById(searchComponent.ids.result),
		//		 searchComponent.events.blur, 
		//%		 searchComponent.controller.blur, true);
		
		eApp.addEvent(document.getElementById(searchComponent.ids.hide),
						 searchComponent.events.click, 
						 function(event) {
							document.getElementById(searchComponent.ids.resultWrapper)
									.className = 'hide';
					  	}, true);		
				 
		
	};
	
	
	function populateItemIds(url) {

		/* populate the item ids, at first check if itemids (express checkout exists
		 *  no time to make it moular :( */
		if (document.getElementById("item_ids")) {
			
			eApp.ajaxRequest(url, "GET", function(responseXML, responseText) {
				
				/* parse the string from Server into JSON Object */
				var serverData = JSON.parse(responseText);
				
				/* now generate the view with the JSON data from Server */
				var itemIdDiv = document.getElementById("item_ids");
				
				var list = serverData.items;
				var resultArr = [];
				for (var i = 0; i < list.length; i++) 
				{
					resultArr.push("<option value=\"" + list[i].number + "\">");
				}
				
				itemIdDiv.innerHTML = resultArr.join("");
				
			}, null);
		}		
		
	}
	
	
	

	eApp.addEvent(window, "load", function() {				
		searchComponent.init(eApp.contextPath + "/search?type=json");
		populateItemIds(eApp.contextPath + "/search?type=json");		
	}, true);
	
})();	
