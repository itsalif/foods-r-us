/**
 * Base Javascript used throughout the App. 
 * jsdoc documentation standard has been followed.
 * 
 * A namespace eApp is create which contains useful functions
 * that is used throughout the App.
 * 
 * -----------------------------------------------------------
 * Best Practices for JavaScript Development has been followed. 
 * Reference:
 * https://developer.mozilla.org/en-US/learn/javascript
 * 
 * 
 * @author Abdullah Rubiyath
 */ 


/**
 * Execute everything inside anonymous function 
 * Uses a single/One Global variable policy 
 */
(function(win) {
	
	"use strict";
	
	/* A single namespace for storing useful functions used throughout the App */
	var eApp = {
		contextPath : null,		
		createXMLHttpRequest: function() {			
			if (window.ActiveXObject) {
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			return new XMLHttpRequest();
		},
	
		/** 
		 * Registers Event listeners across the App
		 * 
		 * @param {DOMElement} elem   The DOM element on which event is to be attached
		 * @param {string} eventType  The type of event (click, hover, keypress etc.)
		 * @param {function} func A function callback to be invoked
		 *  
		 **/
		addEvent: function(elem, eventType, func, capture) {
			
			if(elem.addEventListener) {
				elem.addEventListener(eventType, func, capture ? capture : false);
			} else if (elem.attachEvent) {
				// for IE 
				elem.attachEvent(eventType, func);
			}
		}		
	};
	
	
	/**
	 * Creates an Ajax Request and invokes the callback function
	 * with the returned values from the server.
	 * 
	 * @param {string} url		The Url of the server		
	 * @param {string} method	The method (GET or POST, both are caps)
	 * @param {string} callback	The name of callback function which gets invoked with 
	 *                          reponseXML and responseText received from Server.
	 * @param {string} data		The serialized data to be sent to the server
	 */
	eApp.ajaxRequest = function(url, method, callback, data) {
		
		var xmlHttp = eApp.createXMLHttpRequest();
		
		xmlHttp.open(method, url, true);		
		xmlHttp.onreadystatechange = function() {
			
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				
				/* invoke the callback function with the returned data
				 * from the server */
				callback(xmlHttp.responseXML, xmlHttp.responseText);
				
				/* clean up xmlHttp */
				xmlHttp = null;
			}
		};
		
		if (method == "POST") {
			/* POST request */
			xmlHttp.setRequestHeader(
				"Content-Type", "application/x-www-form-urlencoded"
			);
			
			xmlHttp.send(data);
		}
		else {
			/* GET request */
			xmlHttp.send(null);
		}
	};
	
	/**
	 * Gets all the input tags from a form and makes a serialized data which 
	 * can be used to make request to server from Javascript.
	 * 
	 * @param {String} formId 
	 * 
	 * Note: It does not serialize values of radiobutton and checkbox, which is not 
	 * needed for this project.
	 * 
	 * @param formId	The id of the form
	 */
	eApp.serializeForm = function(formId) {
		var elemList = document.getElementById(formId).getElementsByTagName("input");
		var data = [];
		
		for (elem in elemList) {
			data.push(elem.getAttribute("name") + "=" + 
					  encodeURIComponent(elem.getAttribute("value")));
		}

		data.push("request_type=js");
		return data.join("&");
	};
	
	/* Attach it to window to make it globally accessible */
	win.eApp = eApp;
	
})(window);