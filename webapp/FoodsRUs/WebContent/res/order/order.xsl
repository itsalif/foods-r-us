<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
    <head>
      <title>Purchase Order Report</title>
      <style type="text/css">
	td, th { padding: 5px; }
        h1 { color: #79A618; } h2 { color: #E47911; }
        thead {background-color: #79A618; ; color: #fff; font-weight: normal; }
      	tbody tr:nth-child(even) { background-color: #eee; }
	table {border: solid 1px #ccc; box-shadow: 2px 2px 2px #ccc; }
      </style>
    </head>
    <body>
	<xsl:apply-templates select="order" />
    </body>
    </html>
    </xsl:template>
    <xsl:template match="order">
        <h1><xsl:value-of select="customer/name" /></h1>
	<h3>Account no#: <xsl:value-of select="customer/@account" /></h3>
	<h4>
	   Order Id: <xsl:value-of select="@id" />,
           Submitted: <xsl:value-of select="@submitted" />
	</h4>
	<xsl:apply-templates select="items" />
	<h3>Total: $<xsl:value-of select="./total" /></h3>
	<h4>HST/Tax: $<xsl:value-of select="./HST" /></h4>
        <h2>Grand Total: $<xsl:value-of select="./grandTotal" /></h2>
    </xsl:template>
    <xsl:template match="items">
    <table>
    <thead>
	<tr>
	   <th>Item #</th>
	   <th>Item Name</th>
	   <th>Price</th>
	   <th>Quantity</th>
           <th>Extended</th>	
	</tr>
    </thead>  	
    <tbody>
       <xsl:apply-templates select="item" />
    </tbody>
    </table>
     	
    </xsl:template>	
    <xsl:template match="item">
    <tr>
	<td><xsl:value-of select="@number" /></td>
	<td><xsl:value-of select="./name" /></td>
	<td>$<xsl:value-of select="./price" /></td>
	<td><xsl:value-of select="./quantity" /></td>
	<td>$<xsl:value-of select="./extended" /></td>    
    </tr>
    </xsl:template>

</xsl:stylesheet>

