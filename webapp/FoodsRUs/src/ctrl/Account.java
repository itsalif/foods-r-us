package ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.Constants;
import model.Client;
import model.ClientBean;
import model.Order;

/**
 * Controller that deals with User login and login
 * Url-pattern it deals with are:
 * 
 * /account/		-> User login/or Home Page
 * /account/login	-> POST request to try to login user
 * /account/logout  -> GET request to logout a registered user
 * 
 */
@WebServlet(urlPatterns = {"/account/*"})
public class Account extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
		RequestDispatcher rd;
		HttpSession session = request.getSession(true);
		
		/* check if user wants to logout! */
		if (request.getPathInfo() != null && request.getPathInfo().equals("/logout"))
		{
			Logger.getLogger(Account.class.getName()).log(Level.INFO,
				"USER wants to logout! - so log the user out!");			
				synchronized (session)	{
					session.invalidate();
				}
				
				rd = request.getRequestDispatcher("/res/Login.jspx"); 
				rd.forward(request, response);
				
				return;
		}
		
		if (request.getPathInfo() != null && request.getPathInfo().equals("/success")) 
		{
			/* check if the values exists in session, then store them in request Scope
			 * and remove from session, because they were given after user successfully
			 * purchased the product.
			 */
			if (session.getAttribute(Constants.KEY_ORDER_SUCCESS) != null) 
			{
				request.setAttribute(Constants.KEY_ORDER_SUCCESS, 
										session.getAttribute(Constants.KEY_ORDER_SUCCESS));
				
				request.setAttribute(Constants.KEY_ORDER_URL, session.getAttribute(Constants.KEY_ORDER_URL));
				
				session.removeAttribute(Constants.KEY_ORDER_SUCCESS);
				session.removeAttribute(Constants.KEY_ORDER_URL);
			}
		}
		
		/* if the user is logged in, and pass his order history as well */
		if (session.getAttribute(Constants.KEY_CLIENT) != null) {
			request.setAttribute("order_path", Constants.ORDER_PATH);
			
			Order order = (Order) this.getServletContext().getAttribute("order");
			String orderFilePath = this.getServletContext().getRealPath(Constants.FILE_ALL_ORDERS);
			ClientBean client = (ClientBean) session.getAttribute("client");
			
			try {
				HashMap<String, String> orderUrls = order.getOrderHistory(orderFilePath, Constants.ORDER_PATH + 
												Constants.ORDER_FILE_PREFIX, Constants.ORDER_FILE_SEPARATOR, 
												client.getNumber() + "", Constants.FILE_EXT_XML);
				
				request.setAttribute("ordersMap", orderUrls);
			}
			catch (Exception e) {
				request.setAttribute(Constants.KEY_ERROR_MSG, e.getMessage());
				
			}
			
		}
		
		rd = request.getRequestDispatcher("/res/Login.jspx"); 
		rd.forward(request, response);
	}

	/**
	 * User login request
	 *
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		String basePath = this.getServletContext().getContextPath();
		HttpSession session = request.getSession(true);
		RequestDispatcher rd;
		
		// check to see if the user attempts to login
		if (request.getPathInfo() != null && request.getPathInfo().equals("/login")
			&& request.getParameter("client_login") != null)  
		{
			// Login requested by the user - Get data
			String userName = request.getParameter("client_number");
			String userPass = request.getParameter("client_password");

			// loggedClient is used for storing ClientBean
			ClientBean loggedClient = null;
			
			Logger.getLogger(Account.class.getName()).log(Level.INFO,
					String.format("Login requested. UN: %s, PW: %s", userName, userPass));
			
			try {
				Client clientModel = (Client) this.getServletContext()
											 .getAttribute("clientModel");
				// attempt to login the user
				loggedClient = clientModel.login(userName, userPass);
																
				// synchronize session - as its not thread-safe
				synchronized (session) {
					session.setAttribute(Constants.KEY_CLIENT, loggedClient);
				}
				
				// user logged in, get the user purchase History!
				Order order = (Order) this.getServletContext().getAttribute("order");
				String orderFilePath = this.getServletContext().getRealPath(Constants.FILE_ALL_ORDERS);
				
				// get the orderUrls
				HashMap<String, String> orderUrls =  order.getOrderHistory(orderFilePath, Constants.ORDER_PATH + 
										Constants.ORDER_FILE_PREFIX, Constants.ORDER_FILE_SEPARATOR, 
										loggedClient.getNumber() + "", Constants.FILE_EXT_XML);

				request.setAttribute("ordersMap", orderUrls);
			}
			catch(Exception e) {
				request.setAttribute(Constants.KEY_ERROR_MSG, e.getMessage());
				if (request.getParameter("return_url") != null) {
					request.setAttribute("return_url", request.getParameter("return_url"));
				}
			}
			
			/* return url is needed when user is about to checkout but
			 * is not logged in, so, after login, we simply redirect the user
			 * with cart page again, then user can then checkout and confirm */
			String returnUrl = request.getParameter("return_url");
			if (returnUrl != null && loggedClient != null) {
				response.sendRedirect(basePath + "/cart");
				return;
			}
		}

		rd = request.getRequestDispatcher("/res/Login.jspx");
		rd.forward(request, response);
	}
}
