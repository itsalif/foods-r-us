package ctrl;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.CartModel;
import model.Item;
import model.CartBean;
import model.ClientBean;
import model.Order;
import util.Constants;

/**
 * Servlet implementation class Cart
 */
@WebServlet(urlPatterns = {"/cart/*"})
public class Cart extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Internal logger object to log DEBUG/INFO/WARNING/ERROR messages
	 * 
	 * @see {@link Logger} for more logging options
	 * @see http://wiki.apache.org/tomcat/Logging_Tutorial config and sample
	 */
	private static final Logger logger = Logger.getLogger(Cart.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		this.getServletContext().setAttribute("cartModel", new CartModel());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Debug log request
		String basePath = this.getServletContext().getContextPath();
		HttpSession session = request.getSession(true);
		
		/* check the session for any error message, then set it to request
		 * The reason for this is because on update, I am doing an PRG (Post/Redirect/Get)
		 * to make it more secure, and this it needs to be saved on session!
		 */
		if (session.getAttribute(Constants.KEY_ERROR_MSG) != null) {
			request.setAttribute(Constants.KEY_ERROR_MSG, session.getAttribute(Constants.KEY_ERROR_MSG));
			session.removeAttribute(Constants.KEY_ERROR_MSG);
		}
		
		/* if the path is not null, user is trying to hit confirm, or update etc. 
		 * simply redirect to /cart  */
		if (request.getPathInfo() != null && !request.getPathInfo().equals("/")) {
			response.sendRedirect(basePath + "/cart");
			return;
		}
				
		RequestDispatcher rd = request.getRequestDispatcher("/res/Cart.jspx");		
		rd.forward(request, response);
	}

	/**
	 * Handles shopping cart add/update/delete requests.
	 *
	 * (1) Add - This can be added from virtually any place in the site
	 * (2) Update - User can update quantity or add same item to increase 
	 *     item number 
	 * (3) Delete - User can either delete or select "0" as quantity and update
	 *     their cart to delete item from the cart
	 *
	 * EXPECTED VALUES: <br/>
	 * <ul>
	 * <li> "item_number" - used in add, from any item browse page</li>
	 * <li> "item_quantity" - item quantity value </li>
	 * <li> "item_number[]" - item's unique number IDs </li>
	 * <li> "item_quantity[]" - integer quantity value of respective items </li>
	 * </ul>
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String basePath = this.getServletContext().getContextPath();

		// POST is only accessible if there's path associated (add, update, confirm)
		if (request.getPathInfo() == null) {
			//otherwise, simply redirect to home (someone is trying to sniff form)
			response.sendRedirect(basePath + "/home");
			return;
		}
		
		// Get sessiton & start it if necessary
		HttpSession session = request.getSession(true);
		RequestDispatcher rd;
		CartModel cModel = (CartModel) this.getServletContext().getAttribute("cartModel");
		
		//get the item model
		Item item = (Item) this.getServletContext().getAttribute("itemModel");
				
		if (request.getPathInfo().equals("/add") && 
				request.getParameter("cart_add") != null) 
		{
			/* ensure it came from cart_add button */
			// Add requested
			logger.log(Level.INFO, "Cart - ADD Action Requested");				
			String itemNumber = request.getParameter("item_number");
			String itemQuantity = request.getParameter("item_quantity");
			try {
				CartBean cart;
				// session is not thread safe
				synchronized (session) {
					
					cart = (CartBean) session.getAttribute(Constants.KEY_CART);					
					if (cart == null) {
						// cart is empty - first time
						cart = new CartBean();
						session.setAttribute(Constants.KEY_CART, cart);
					}
					
					cModel.add(cart, item.getItem(itemNumber), itemQuantity);					

					// at this point, item has been successfully added, store the time.
					session.setAttribute(Constants.KEY_ITEM_ADDED, (new Date()).getTime());
				}
			} catch (Exception ex) {
				logger.log(Level.SEVERE, "Cart add error", ex);
				request.setAttribute(Constants.KEY_ERROR_MSG, ex.getMessage());
			}
			
			rd = request.getRequestDispatcher("/res/Cart.jspx");
			rd.forward(request, response);
			
			//response.sendRedirect(basePath + "/cart");
		} 
		else if (request.getPathInfo().equals("/update") && 
				 request.getParameter("cart_update") != null) 
		{
			/* ensure it came from cart_update button */
			// Update the cart with existing items
			logger.log(Level.INFO, "Cart - UPDATE Action Requested");

			// Get list of numbers and quantities
			String[] itemNumbers = request.getParameterValues("item_number[]");
			String[] itemQuantities = request.getParameterValues("item_quantity[]");

			// Debug inputs
			logger.log(Level.INFO, "List of IDs: {0} ", 
					Arrays.toString(itemNumbers));

			logger.log(Level.INFO, "List of Quantities: {0} ", 
					Arrays.toString(itemQuantities) );

			// Now, we have values, try to update cart
			try {
				CartBean cart;
				synchronized (session) {
					cart = (CartBean) session.getAttribute(Constants.KEY_CART);
					if (cart == null) {
						// cart is empty - first time
						cart = new CartBean();
						session.setAttribute(Constants.KEY_CART, cart);
					}
					cModel.update(cart, item.getItems(itemNumbers), itemQuantities);
				}
			} catch (Exception ex) {
				logger.log(Level.SEVERE, "Cart add error", ex.getMessage());
				session.setAttribute(Constants.KEY_ERROR_MSG, ex.getMessage());
			}
			
			/* redirection instead of forwarding is necessary to prevent users
			 * from making repeated purchases (by refreshing or hitting F5) */
			response.sendRedirect(basePath + "/cart");				
		}
		else if(request.getPathInfo().equals("/checkout") &&
				request.getParameter("cart_checkout") != null) 
		{
			// before asking to confirm, ensure that user is logged in
			synchronized (session) {
				ClientBean user = (ClientBean) session.getAttribute("client");
				if (user == null) {
					//user is not logged in, so 
					
					request.setAttribute("user_login_required", 
							Constants.MESSAGE_LOGIN_REQUIRED);
					
					request.setAttribute("return_url", 
							basePath + "/cart");
					
					rd = request.getRequestDispatcher("/res/Login.jspx");
					rd.forward(request, response);
					return;
				}
				
			}
			
			rd = request.getRequestDispatcher("/res/Confirm.jspx");
			rd.forward(request, response);
		}
		else if(request.getPathInfo().equals("/confirm") &&
				request.getParameter("cart_confirm") != null) 
		{
			// process/complete the order and then redirect user to a success page!
			// check if user is not logged in, session is not thread-safe, so lock it
			synchronized (session) {
				
				CartBean cart = (CartBean) session.getAttribute(Constants.KEY_CART);
				ClientBean user = (ClientBean) session.getAttribute("client");
				
				if (cart != null && user != null) {

					// place order ?						
					Order order = (Order) this.getServletContext().getAttribute("order");
					try {
					
					/* get the realPath and then get the total count and customer's total count
					 * and then increment each of them by 1 (to get the current) */
					String allOrdersXmlPath = this.getServletContext().getRealPath(Constants.FILE_ALL_ORDERS);
					
					
					int orderCount = order.getOrderCount(allOrdersXmlPath, user.getNumber() + "");
					orderCount++;
					
					/* create a proper order file */
					String orderPath = Constants.ORDER_PATH + Constants.ORDER_FILE_PREFIX
										+ user.getNumber() + Constants.ORDER_FILE_SEPARATOR
										+ orderCount  + Constants.FILE_EXT_XML;
										
					String xmlFilePath = this.getServletContext().getRealPath(orderPath);
					
					Logger.getLogger(Cart.class.getName())
							  .log(Level.SEVERE, "file: " + xmlFilePath);
					
						// create the new order with the new order Count
						order.create(orderCount, xmlFilePath, user, cart);
						
						// now update it in the all orders form
						order.updateAllOrders(allOrdersXmlPath, user.getNumber() + "", orderCount);
						
						/* store the number of items set in session */
						session.setAttribute(Constants.KEY_CHECKOUT_ITEMS_COUNT, cart.getCartItems().size());
							
						// and then remove cart from session but keep the user!
						session.removeAttribute(Constants.KEY_CART);
						session.setAttribute(Constants.KEY_ORDER_SUCCESS, Constants.MESSAGE_ORDER_SUCCESS);
						session.setAttribute(Constants.KEY_ORDER_URL, orderPath);						
						
						/* redirection instead of forwarding is absolutely necessary
						 * otherwise, a user can easily hit F5/Refresh and keep
						 * making purchases repeatedly */
						response.sendRedirect(basePath + "/account/success");

					} catch (Exception ex) {
						Logger.getLogger(Cart.class.getName())
							  .log(Level.SEVERE, null, ex);
					}
				}				
				else {
					// cart is empty/null, i guess forward to cart
					rd = request.getRequestDispatcher("/res/Cart.jspx");
					rd.forward(request, response);
				}
			}
		}
		else {
			// all other cases, simply redirect to Home Page
			response.sendRedirect(basePath + "/home");
		}
			
	}
}
