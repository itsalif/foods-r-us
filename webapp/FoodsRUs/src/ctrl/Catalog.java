package ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.Constants;
import model.CategoryBean;
import model.Client;
import model.Item;
import model.ItemBean;
import model.Order;

/**
 * A Controller that manages request related to Catalog (Category of Items) and
 * that of an individual Item.
 * 
 * The Path it corresponds to includes:
 *
 * home -> Home / Landing Page of the site
 * browse/ -> Listing of all items in the database
 * browse/[cat-id] -> list of all items in database with category id = [cat-id]
 * /search?q=query -> Lists all item with name or number matching 'query'
 * 
 * 
 * @author Abdullah Rubiyath
 * 
 */
@WebServlet(urlPatterns = {"/browse/*"}, loadOnStartup = 0)
public class Catalog extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creating logger instance for debug/warn/error logs
	 */
	public static final Logger logger = Logger.getLogger(Catalog.class.getName());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Catalog() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {

    	super.init();    	
    	try {
    		Item item = new Item();
    		
    		/* store the itemModel, clientModel and order from this controller
    		 * as this controller is loaded the first */
    		this.getServletContext().setAttribute("itemModel", item);
			this.getServletContext().setAttribute("clientModel", new Client());    		
			this.getServletContext().setAttribute("order", new Order());
			
			/* Category is stored in the Application Context for performance.
    		 * Since Category is required in all parts of the Website thus, 
    		 * the need of an extra query to database in every page to get
			 * Category is unnecessary, so, it stored in App Context. */
    		this.getServletContext().setAttribute("categoriesList", 
													item.getCategories());
		} 
    	catch (Exception e) {
			logger.log(Level.SEVERE, "Unable to init", e);
			throw new ServletException("Model Exception", e);
		}
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		// Debug log request
		Logger.getLogger(Catalog.class.getName()).log(Level.INFO, 
				String.format("Catalog HTTP GET - Servlet Path %s, Path info: %s, Context Path: %s", 
				request.getServletPath(), request.getPathInfo(), request.getContextPath() ));
		
		RequestDispatcher rd;
		
		//home page (default)
		if (request.getServletPath().equals("/home")) 
		{
			/**
			 * Get Products/Items from every Category as its the Home Page. 
			 * Note: All methods inside Item Model is thread-safe/synchronized.
			 */
			try {
				Item item = (Item) this.getServletContext().getAttribute("itemModel");
				HashMap<CategoryBean, List<ItemBean>> itemsExcerptMap = 
								item.getItemsExcerptFromCategories();		
				request.setAttribute("itemsMap", itemsExcerptMap);	
				
				System.out.println("Items Size: " + itemsExcerptMap.size());
			}
			catch(Exception e) {
				request.setAttribute(Constants.KEY_ERROR_MSG, e.getMessage());
				logger.log(Level.SEVERE, "Unable to list items.", e);
			}
			
			rd = request.getRequestDispatcher("/res/Home.jspx");
			rd.forward(request, response);
		}
		else if (request.getServletPath().equals("/browse")) 
		{
			// show all the items irrespective of category or belonging to a category!			
			
			/* do a check if path is just '/', then set category to null as
			 * no category is specified by user	 */
			String category;			
			if (request.getPathInfo() == null || request.getPathInfo().equals("/")) {
				category = null;
			}
			else {
				/* get the id from the Path Info */
				category = this.getIdFromPath(request.getPathInfo());
			}
		
			try {
								
				Item item = (Item) this.getServletContext().getAttribute("itemModel");
				List<ItemBean> itemsList = item.getItems(category);	
				request.setAttribute("itemsList", itemsList);
				
				CategoryBean categoryBean = item.getCategory(category);
				request.setAttribute("category", categoryBean);
				
			}
			catch(Exception e) {
				request.setAttribute(Constants.KEY_ERROR_MSG, e.getMessage());
				logger.log(Level.SEVERE, "Catalog Exception: " + e, e);
			}
			
			rd = request.getRequestDispatcher("/res/Catalog.jspx");
			rd.forward(request, response);
			
		}
		else if (request.getServletPath().equals("/search")) {
            /*
             * Search controller
             */
			final String jsonFile = "res/cache/items.json";
			final String KEY_HTTP_GET_DATA_TYPE = "type";
			
			// First, check if the request is from auto-suggest, then serve cached content
			if(request.getParameter(KEY_HTTP_GET_DATA_TYPE)!= null 
					&& request.getParameter(KEY_HTTP_GET_DATA_TYPE).equals("json")){

				String exportFile = this.getServletContext().getRealPath(jsonFile);
				
				logger.log(Level.INFO, "JSON File: {0}", exportFile);
				
				try {
					Item item = (Item) this.getServletContext().getAttribute("itemModel");					
					item.exportItems(exportFile);
					
				} catch (Exception ex) {
					
					Logger.getLogger(Catalog.class.getName()).log(Level.SEVERE, null, ex);
				}
				
				// Finally send the cached (or generated on first time) content as response
				rd = request.getRequestDispatcher(jsonFile);
				rd.forward(request, response); //forward to JSON file
				
			} else {
			

				try {
 
					Item item = (Item) this.getServletContext().getAttribute("itemModel");
					String userQuery = request.getParameter("q");
					List<ItemBean> searchResultItems = item.getSearchItems(userQuery);
					
					logger.log(Level.INFO, "Total search {0} results found.", searchResultItems.size());
					
					request.setAttribute("itemsList", searchResultItems);
					request.setAttribute("itemsSize", searchResultItems.size());
					request.setAttribute("q", userQuery);					
				} catch (Exception ex) {
					logger.log(Level.SEVERE, "Search failed.", ex);
					request.setAttribute(Constants.KEY_ERROR_MSG, ex.getMessage());
				}

				request.setAttribute("isSearch", true);
				rd = request.getRequestDispatcher("/res/Catalog.jspx");
				rd.forward(request, response);
			}
		}
		else {
			/* path contains category id and or item-id */
			//if time permits, then implement this feature
			// create a separate page for an Item
//			System.out.print("Category List > ");
			
			System.out.println();
			
		}
		System.out.println("Servlet Path: " + request.getServletPath());
		// if request path is only "/" then list a browse of all items
		
		/**
		 * @TODO: Add Model Code for a selected Category! 
		 */
	//	rd = request.getRequestDispatcher("/res/Category.jspx");
	//	rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	
	/**
	 * Removes the slashes and returns an Id from Path
	 * For example:
	 * On a Path /browse/178/
	 * 
	 * If this method is invoked with /178/, then 
	 * 178 is returned, i.e. the slashes are removed.
	 * 
	 */
	private String getIdFromPath(String path) {
		return path.replace("/", "");
	}

}
