package listener;


import java.util.Date;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import util.Constants;

/**
 * Application Lifecycle Listener implementation class AnalyticsListener
 * 
 * Keeps track of the time
 *
 */
@WebListener
public class AnalyticsListener implements HttpSessionListener, HttpSessionAttributeListener {
	
	
    /**
     * Default constructor. 
     */
    public AnalyticsListener() {
        // TODO Auto-generated constructor stub

    }

	/**
     * @see HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
     */
    public void attributeRemoved(HttpSessionBindingEvent arg0) {
        // TODO Auto-generated method stub
    	// useful for checking-out, when cart was removed from Session!
    }

	/**
     * @see HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
     */
    public void attributeAdded(HttpSessionBindingEvent arg0) {
        // TODO Auto-generated method stub
    	
    	this.updateCartTime(arg0, true);    	
    	
    	/* check if the attribute added was KEY_CART i.e. cart 
    	if (attributeAdded.equalsIgnoreCase(Constants.KEY_CART)) 
    	{
        	HttpSession session = arg0.getSession();    		
    		/* lock the session, as session is not thread safe 
    		synchronized (session) {
        		/* get the time it took to add item to cart from creation time 
            	long currentTime = (new Date()).getTime();
            	long timeDiff = currentTime - session.getCreationTime(); 
            	
            	Long runningTime = (Long) session.getServletContext()
            								.getAttribute(Constants.KEY_TOTAL_TIME);
            	
            	Integer totalPeople = (Integer) session.getServletContext()
            									.getAttribute(Constants.KEY_TOTAL_NUM);
            	// first time
            	if (runningTime == null) {
            		runningTime = new Long(0);
            		totalPeople = 0;
            	}
            	runningTime += timeDiff;
            	totalPeople += 1;
            	
            	session.setAttribute(Constants.KEY_TOTAL_TIME, runningTime);
            	session.setAttribute(Constants.KEY_TOTAL_NUM, totalPeople);
            	
            	System.out.println("Cart Added: " + totalPeople + " | " + runningTime);
    		}
    	}
    */
    }

	/**
     * @see HttpSessionAttributeListener#attributeReplaced(HttpSessionBindingEvent)
     */
    public void attributeReplaced(HttpSessionBindingEvent arg0) {
        // TODO Auto-generated method stub
    	this.updateCartTime(arg0, false);
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent arg0) {
        // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent arg0) {
        // TODO Auto-generated method stub
    }
	
    /**
     * Tracks the Adding to Cart Time
     * @param arg0
     */
    private synchronized void updateCartTime(HttpSessionBindingEvent arg0,
    										 boolean firstTime) 
    {
    	String attributeChanged = arg0.getName();
    	HttpSession session = arg0.getSession();
    	
    	/* if someone completed shopping/checked out from the shop! */
    	if (attributeChanged.equalsIgnoreCase(Constants.KEY_CHECKOUT_ITEMS_COUNT)) {
    		
    		// add the total running time in servletContext!
    		Long totalTime = (Long) session.getServletContext()
    								  .getAttribute(Constants.KEY_TOTAL_TIME);

    		Integer totalNum = (Integer) session.getServletContext()
					  							.getAttribute(Constants.KEY_TOTAL_NUM);
    		
    		if (totalTime == null) {
    			totalTime = (long) 0;
    			totalNum = 0;
    		}
    		
    		totalTime += (new Date()).getTime() - session.getCreationTime();
    		totalNum += (Integer) session.getAttribute(Constants.KEY_CHECKOUT_ITEMS_COUNT);
    		
    		session.setAttribute(Constants.KEY_TOTAL_TIME, totalTime);
    		session.setAttribute(Constants.KEY_TOTAL_NUM, totalNum);
    		
    	}
    }
}
