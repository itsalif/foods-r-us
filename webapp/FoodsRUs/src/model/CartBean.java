package model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Bean that holds the items and its quantity in cart
 * 
 * @author Abdullah Rubiyath
 */
public class CartBean
{

	/**
	 * A ConcurrentMap that stores Items as ItemBean, and 
	 * a corresponding Integer storing the number of that
	 * items being stored in the Shopping Cart.  
	 * 
	 * @see {@link ConcurrentHashMap}
	 */
    private ConcurrentHashMap<ItemBean, Integer> cartItems;
	
	/**
	 * The need for a separate Items is needed because
	 * cartItems.size() do not reflect the actual 
	 * number of Items in the cart. 
	 */
	private int itemsCount;
	
	/**
	 * The total tax for the amount in the Cart
	 */
	private double tax;
	
	private double total;
	
	/**
	 * The shipping charge
	 */
	private int shipping;
	
	/**
	 * The Grand Total = (total + shipping + tax)
	 */
	private double grandTotal;
	
	/**
	 * Constructs an empty Shopping Cart
	 */
	public CartBean() {
		this.cartItems = new ConcurrentHashMap<ItemBean, Integer>();
		this.itemsCount = 0;
		this.total = 0;
		this.tax = 0;
		this.grandTotal = 0;
		this.shipping = 0;
	}
	
	
	/**
	 * Returns the total number of items in shopping Cart
	 * @return 
	 */
	public int getQuantity() {
		return this.itemsCount;
	}
	

	
	/**
	 * Clears/Empties the cart
	 */
	public void clear() 
	{
		this.cartItems = null;
		this.itemsCount = 0;
	}
	
	/**
	 * Returns the Cart Items Map.
	 */
	public Map<ItemBean, Integer> getCartItems() {
		return this.cartItems;
	}
	
	/**
	 * Returns the shipping cost
	 */
	public int getShipping() {
		return this.shipping;
	}
	
	/**
	 * Returns the shipping cost
	 */
	public void setShipping(int s) {
		this.shipping = s;
	}	
	
	/**
	 * Returns the Total
	 * @return 
	 */
	public double getTotal() {
		return this.total;
	}
	
	/**
	 * Returns the shipping amount
	 * @return 
	 */
	public double getTax() {
		return this.tax;
	}	
	
	/**
	 * Set the Total 
	 */
	public void setTotal(double t) {
		this.total = t;
	}
	
	/**
	 * Sets the Tax
	 */
	public void setTax(double t) {
		this.tax = t;
	}		
	
	/**
	 * Returns the Grand Total
	 * @return 
	 */
	public double getGrandTotal() {
		return this.grandTotal;
	}
	
	/**
	 * Returns the Grand Total
	 * @return 
	 */
	public void setGrandTotal(double newTotal) {
		this.grandTotal = newTotal;
	}	
	
	/**
	 * 
	 * @return
	 */
	public int getItemsCount() {
		return this.itemsCount;
	}
	
	
	public void setItemsCount(int t) {
		this.itemsCount = t;
	}	
	
}
