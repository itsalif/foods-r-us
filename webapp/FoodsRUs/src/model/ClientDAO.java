package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;



import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * DAO to access the client database
 * @author cse73194
 *
 */
public class ClientDAO
{
	private DataSource dataSource;
	
	/**
	 * 
	 * @throws Exception
	 */
	public ClientDAO() throws Exception
	{

		this.dataSource = (DataSource) (new InitialContext())
											.lookup("java:/comp/env/jdbc/EECS");

	}


	/**
	 * Attempts to retrieve the Client with accountNumber and password
	 * from the database.
	 * 
	 * @param	accountNumber	The accountNumber of the Client
	 * @param	password		The password of the Client	
	 * @return	The corresponding ClientBean with matching accountNumber & password
	 *			If no match is found, then null is returned	 
	 * 
	 * @throws	Exception
	 */
	public ClientBean retrieveClient(String accountNumber, String password) 
			throws Exception
	{
		Connection con = this.getConnection();
		Statement s = con.createStatement();
		this.setDatabaseSchema(s);
		
		/* Note: This course does not permit PreparedStatement,
		 * but seriously PreparedStatement is the most effective way
		 * to execute Queries and prevent SQL Injection! */
		String query = " SELECT name, number, rating FROM client " +
					   " WHERE number=" + accountNumber + 
					   " AND password='"+ password + "'";
		
		ResultSet r = s.executeQuery(query);
		ClientBean clientB = null;
		if (r.next()) {
			clientB = new ClientBean(r.getInt("NUMBER"), r.getString("NAME"));			
		}
		
		con.close();

		return clientB;
	}
	
	/**
	 * sets the schema to roumani in the database
	 * @param s
	 * @throws Exception
	 */
	private void setDatabaseSchema(Statement s) throws Exception
	{
		s.executeUpdate("set schema roumani");
	}
	
	/**
	 * Returns the connection from datasource
	 */
	private Connection getConnection() throws Exception 
	{
		//if (false == true) {
		//	Class c   = Class.forName("org.apache.derby.jdbc.ClientDriver");
		//	return DriverManager.getConnection("jdbc:derby://indigo.cse.yorku.ca:9999/CSE",
		//			"student" , "secret");
	//	}
		return this.dataSource.getConnection();
	}	
}
