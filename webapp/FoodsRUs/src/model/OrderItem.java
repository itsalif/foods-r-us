package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import util.ToolBox;

/**
 * OrderItem Class which contains all the information of an Order,
 * it is used by JAXB to marshall (generate XML) 
 * 
 * @author Abdullah Rubiyath
 */

@XmlRootElement(name="item")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItem {
	
	@XmlAttribute
	private String number;
	private String name;
	private double price;
	private int quantity;
	private String extended;

	public OrderItem() {}
	
	public OrderItem(ItemBean item) {

		this.number = item.getNumber();
		this.name = item.getName();
		this.price = ToolBox.formatDigits(item.getPrice());
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the extended
	 */
	public String getExtended() {
		return extended;
	}

	/**
	 * @param extended the extended to set
	 */
	public void setExtended(double extended) {
		this.extended = ToolBox.formatDigits(extended) + "";
	}
		
		
		
}

