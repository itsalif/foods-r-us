package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * ClientBean for creating object of type Client and getting its attributes.
 * 
 * @author cse73194
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ClientBean
{
	@XmlAttribute(name="account")
	private int number;
	
	@XmlElement
	private String name;
	
	public ClientBean() {
		
	}
	
	public ClientBean(int number, String name)
	{
		this.number = number;
		this.name = name;
	}


	public int getNumber()
	{
		return number;
	}

	public String getName()
	{
		return name;
	}
	
	@Override
	public String toString() {
		return "ClientBean [number=" + number + ", name=" + name + "]";
	}	
}
