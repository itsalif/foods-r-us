/*
 */
package model;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * A Model that is used to generate Order XML and get the order Count from all
 * the orders made in the Shop so far. It reads from XML file 
 * which is to be on the following format:
 * 
 * <pre>
 * 
 * <?xml ..>
 * <orders>
 * 	<order id="ORDER_ID" customer="CUSTOMER_ID"  
 * 		   cid="CUSTOMER_ORDER_ID" status="0 or 1" />
 * </orders>
 * 
 * </pre>
 * 
 * In certain methods (getOrdersCount, getCustomersOrderCount), XPath
 * was used to get total Contents instead of JAXB unmarshalling,  
 * because JAXB requires creating another Object which seems like an 
 * overkill to just get the count, so XPath has been used.
 * 
 * @author Abdullah Rubiyath
 * 
 */
public class Order extends BaseModel {
	
	
	public Order() {
		
	}
	
	/**
	 * Gets the Order Count of an individual customer in the Shop
	 * 
	 * @param fileName the filename to be parsed from
	 * @return
	 * @throws Exception
	 */
	public synchronized int getOrderCount(String fileName, String clientId) 
			throws Exception {
		
	    DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	    domFactory.setNamespaceAware(true); 
	    DocumentBuilder builder = domFactory.newDocumentBuilder();
	    
	    Document doc = builder.parse(fileName);
	    
	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile("count(//order[@customer='" + clientId + "'])");
	    
	    /* get the count of total number of orders for a specific customer */
	    Double total = (Double) expr.evaluate(doc, XPathConstants.NUMBER);
	    
	    System.out.println("Total Clients >> " + total);
	    
	    return total.intValue();
	}
	
	
	/**
	 * Adds a new Order to the Global (all_orders.xml) file in the Shop
	 * TODO create another Object and use JAXB to Marshall/update the XML file
	 * as in that approach the tags/elements wont be hardcoded like now.
	 * 
	 * @param xmlFile		The XML File to write the order
	 * @param clientNumber	The client Number
	 * @param orderId		The Order Id of the client
	 * @param status		The status (0 -> for pending, 1 -> completed)
	 * 
	 */
	public synchronized void updateAllOrders(String xmlFile, String clientNumber, 
								int orderId) throws Exception {
		// make an order date
		String orderDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		
		System.out.println("XML File >>" + xmlFile);
		
		// load the XML file at first
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile);
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("orders");		
		
		// create an order element and add id, customer, cid, status to it
		Element orderElem = doc.createElement("order");		
		orderElem.setAttribute("id", orderId + "");
		orderElem.setAttribute("customer", clientNumber + "");
		orderElem.setAttribute("submitted", orderDate + "");
		
		// append/add it to existing orders (as its child)
		nList.item(0).appendChild(orderElem);
		
		
		// now its time to update/rewrite it back to file, create a DOMSource
		DOMSource source = new DOMSource(doc);

		// use the Transformer to transform it and write source back to file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult(xmlFile);                
        transformer.transform(source, result);		
	}
	
	
	
	/**
	 * Create/Make the Order file for a Client
	 * 
	 * @param orderId	The Order Id
	 * @param fileName	The fileName (xml file) where order is to be written
	 * @param client	The ClientBean which contains the User's info.
	 * @param cart		The CartBean which contains the orders
	 */
	public synchronized void create(int orderId, String fileName, ClientBean client, 
					   CartBean cart) throws Exception {
		
		String orderDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		OrderWrapper ow = new OrderWrapper(orderId, orderDate, cart, client);
		
		JAXBContext jc  = JAXBContext.newInstance(OrderWrapper.class);
				
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

		StringWriter sw = new StringWriter();
		sw.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
		sw.write("<?xml-stylesheet type=\"text/xsl\" href=\"order.xsl\"?>\n");
		marshaller.marshal(ow, new StreamResult(sw));
		
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		
		//System.out.println(sw.toString());
		FileWriter fw = new FileWriter(fileName);
		fw.write(sw.toString());
		fw.close();		
	}
	
	
	/**
	 * Gets/Returns all the orders of a specific client/customer.
	 * Returns a HashMap<String,String> of order id with its Url Strings,
	 * each Url String referring to an ordered/existing file
	 * 
	 * @param fileName		The fileName which is to be processed (all_orders.xml)
	 * @param prefix		The Prefix (usually _po)
	 * @param separator		The Separator (_)
	 * @param clientId		The id of the client
	 * @param ext			The file extension
	 * @return
	 * @throws Exception
	 */
	public synchronized HashMap<String, String> getOrderHistory(String fileName, String prefix, 
										String separator, String clientId, String ext) throws Exception {
		
		/* gets all the orders of a specific client */
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	    domFactory.setNamespaceAware(true); 
	    DocumentBuilder builder = domFactory.newDocumentBuilder();
	    
	    Document doc = builder.parse(fileName);
	    
	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile("//order[@customer='" + clientId + "']");
	    
	    /* get the count of total number of orders for a specific customer */
	    NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
	    
	    /* create a Map<String,String> for storing all urls of the client's orders */
	    HashMap<String, String> urls = new HashMap<String, String>();
	    String temp;
	    String orderId;
	    
	    for (int i = 0; i < nodeList.getLength(); i++) {
	    	orderId = nodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
	    	temp = prefix + clientId + separator + orderId + ext;
	    	urls.put(orderId, temp);
	    }
	    
	    return urls;	    
	}
	
}
