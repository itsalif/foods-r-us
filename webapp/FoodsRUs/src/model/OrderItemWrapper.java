/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A Wrapper for Order Item, needed as the order item are wrapped by another items
 * tag. Used by JAXB to generate/marshall XML content. 
 * 
 * @author Abdullah Rubiyath
 */
@XmlRootElement(name="items")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItemWrapper {
	
	@XmlElement(name="item")
	private List<OrderItem> items;
	
	public OrderItemWrapper() {
		this.items = new ArrayList<OrderItem>();		
	}
	
	
	public void add(OrderItem newItem) {
		this.items.add(newItem);
	}
	
	
	public List<OrderItem> getItems() {
		return this.items;
	}
}
