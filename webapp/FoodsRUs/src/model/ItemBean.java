package model;

/**
 * ItemBean for communicating across the Web App
 * @author cse73194
 * 
 */

public class ItemBean
{
	private String number;
	
	private String name;
	
	private double price;
	
	private int qty;
	
	private String unit;
	
	
	public ItemBean(String number, String itemName, int itemQty, 
					double itemPrice, String unit) 
	{	
		this.number = number;
		this.name = itemName;
		this.price = itemPrice;
		this.qty = itemQty;
		this.unit = unit;
	}
	
	public String getNumber()
	{
		return this.number;
	}

	public String getName()
	{
		return this.name;
	}

	public int getQty()
	{
		return this.qty;
	}

	public double getPrice()
	{
		return this.price;
	}
	
	public String getUnit() 
	{
	    return this.unit;
    }
	
	@Override
	public int hashCode() {
		return this.number.hashCode();
	}
	
	@Override
	public boolean equals(Object b) {
		if (b != null && b instanceof ItemBean) {
			// item numbers are unique, so if they are equal, they are equal
			ItemBean bean = (ItemBean) b;			
			return this.number.equals(bean.number);
		}
		
		return false;
	}
	
	
	/**
	 * Returns JSON object representation of current {@link ItemBean} object
	 * 
	 * @return JSON Object string
	 */
	public String toJsonString(){
		return String.format(
				"{\"number\":\"%s\",\"name\":\"%s\"}", 
				this.number, this.name); // TODO: Filter quote, slash etc
	}
	

	
}
