package model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author
 *
 */
public class Item
{
	private ItemDAO dao;
	
	/**
	 * The Types of Sanitization
	 * INTEGER OR STRING
	 */
	private enum SanitizeTypes {
		STRING, INTEGER
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public Item() throws Exception {
		this.dao = new ItemDAO();
	}
	
	
	/**
	 * Gets the list of all Categories in the database and returns
	 * them as a List of CategoryBean.
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public synchronized List<CategoryBean> getCategories() throws Exception {
		return this.dao.retrieveCategories();
	}
	
	
	/**
	 * Gets a HashMap of all CategoryBean with its Id# (unique) used as a Key
	 * in the Map. It invokes the other method (getCategories) and then 
	 * iterates over it to create a HashMap of Category's key mapped to CategoryBean.
	 * 
	 * @return A HashMap<Integer, CategoryBean> consisting of Category Id as its key
	 * 					and the CategoryBean corresponding to the Id.
	 * 
	 * @throws Exception
	 */
	public synchronized Map<Integer, CategoryBean> getCategoriesMap() throws Exception {
		List<CategoryBean> categoryList =  this.dao.retrieveCategories();
		HashMap<Integer, CategoryBean> categoryMap = new HashMap<Integer, CategoryBean>();
		
		for(CategoryBean c: categoryList) {
			categoryMap.put(c.getId(), c);
		}
		
		return categoryMap;
	}	
	
	
	/**
	 * Invokes the other method (getItems) with 0 as numberOfRecords, thus
	 * returning all the records.
	 * 
	 * @param  categorgy      The Category Id from which the items will be returned.
	 *                        If null is provided as category, it returns ALL items
	 *                        from database, irrespective of category.
	 *                          
	 * @return List<ItemBean> A List of the Items
	 * 
	 * @throws Exception	  
	 */
	public synchronized List<ItemBean> getItems(String category) throws Exception {
		return this.getItems(category, 0);
	}

	
	/**
	 * Gets all the Items that belongs to a specific category. If null is
	 * provided as category, then it returns all items (irrespective of category)
	 * 
	 * @param  categorgy      The Category Id from which the items will be returned.
	 *                        If null is provided as category, it returns ALL items
	 *                        from database, irrespective of category.
	 * 
	 * @param numberofRecords The number of Records to be fetched from database.                        
	 *                          
	 * @return List<ItemBean> A List of the Items
	 * 
	 * @throws Exception	  
	 */
	public synchronized List<ItemBean> getItems(String category, int numberOfRecords) 
			throws Exception {
		
		/* sanitize the input and then retrieve from the model */
		String catId = category == null ? category
					 	: this.sanitizeInput(category, SanitizeTypes.INTEGER);		
		
		return this.dao.retrieveCategoryItems(catId, numberOfRecords);	
	}	

	
	/**
	 * Gets Items from all Categories, and then puts then in a HashMap, where
	 * each Key corresponds to Category, and its value corresponds to the Items
	 * in the Categories.
	 * 
	 * Originally intended to return only few Items from each Categories,
	 * however, Derby Database does not support LIMIT, so, all items are 
	 * being returned.
	 * 
	 * @see
	 * http://db.apache.org/derby/faq.html#limit
	 * 
	 * @return A HashMap<CategoryBean, List<ItemBean>> consisting of Items from each Category
	 */
	public synchronized HashMap<CategoryBean, List<ItemBean>> getItemsExcerptFromCategories() 
			throws Exception {
		
		final int MAX_ITEMS_RECORD = 3;
		
		HashMap<CategoryBean, List<ItemBean>> itemList = 
							   new HashMap<CategoryBean, List<ItemBean>>();
		
		List<CategoryBean> categories = this.getCategories();
		
		// @TODO: optimize accessing of items from categories!
		for(CategoryBean c: categories) {
			List<ItemBean> list = this.getItems(c.getId() + "", MAX_ITEMS_RECORD); 
			itemList.put(c, list);
		}
		
//		System.out.println("Item Size: " + itemList.size());
		
		return itemList;
	}
	
	
	
	
	/**
	 * Sanitizes an input to prevent it from SQL Injection. 
	 * At first slashes from the string (if there's any), and 
	 * then it sanitizes input. 
	 * 
	 * @param input The input to be sanitized,, Input format is: [/]integer-id[/]
	 * 				Note: both slashes are optional.
	 * 
	 * @param type  Type is either STRING or INTEGER
	 * 
	 * @return The Sanitized String
	 */
	private String sanitizeInput(String input, SanitizeTypes type) {
				
		if (type == SanitizeTypes.INTEGER) 
		{
			/* Attempt to parse the input to an integer and if 
			 * no exception is thrown, then simply return the 
			 * parsed Integer as a String
			 */				
			int number = Integer.parseInt(input);
			return String.valueOf(number);			
		}
		else {
			
			/* remove anything thats not a character (a-z A-Z) 
			 * or a digit(0-9) or a space. 
			 */
			return input.replaceAll("[^A-Za-z0-9 ]", "");
			
		}		
	}
	
	
}
