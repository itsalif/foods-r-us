package model;

/**
 * Client model for communicating with ClientDAO
 * 
 * @author cse73194
 */
public class Client extends BaseModel {
	
	/* error message associated with this model */
	private final String ERROR_MESSAGE = "Invalid Account Number or Password";
	
	/* dao for accessing/communicating with database */
	private ClientDAO dao;
	
	public Client() throws Exception {
		this.dao = new ClientDAO();
	}
	
	/**
	 * logs a Client with the provided number and password. 
	 * @param number   The Account number of client
	 * @param password The Password of the client 
	 * @return ClientBean
	 */
	public synchronized ClientBean login(String number, String password)
		throws Exception 
		{
		
		// sanitize both input 
		try {
			number = super.sanitizeInput(number, SanitizeTypes.INTEGER);
			password = super.sanitizeInput(password, SanitizeTypes.STRING);
		}
		catch(NumberFormatException e) {
			/* This clause is executed if user enters a non-integer for account no.
			   in which case we display error without going to db. */			   
			throw new Exception(ERROR_MESSAGE);
		}
		
		ClientBean cb = this.dao.retrieveClient(number, password);
		
		/* if no valid username/password was provided */
		if (cb == null) {
			throw new Exception(ERROR_MESSAGE);
		}
			
		return cb; 
	}
	
	
}
