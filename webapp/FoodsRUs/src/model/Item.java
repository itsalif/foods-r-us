package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * All methods in Item are thread-safe/synchronized.
 * @author cse73194
 *
 */
public class Item extends BaseModel
{
	private ItemDAO dao;
	
	/* Error Messages specific */
	private final String ERROR_MESSAGE_ITEM_NOT_FOUND = 
			"There is no item with the item number specified. Please Try again!";
	
	/* Error Messages specific */
	private final String ERROR_MESSAGE_CATEGORY_NOT_FOUND = 
			"There is no category with the category number specified. Please Try again!";
	
	/**
	 * 
	 * @throws Exception
	 */
	public Item() throws Exception 
	{
		this.dao = new ItemDAO();
	}
	
	
	/**
	 * Gets the list of all Categories in the database and returns
	 * them as a List of CategoryBean.
	 * 
	 * @return
	 */
	public synchronized List<CategoryBean> getCategories() throws Exception 
	{
		return this.dao.retrieveCategories();
	}
	
	/**
	 * Gets the Information as CategoryBean of a specific Category
	 * 
	 * @param cat
	 * @return
	 */
	public synchronized CategoryBean getCategory(String cat) throws Exception 
	{
		if (cat == null) return null;
		cat = super.sanitizeInput(cat, SanitizeTypes.INTEGER);
		return this.dao.retrieveCategory(cat);
	}
	
	
	/**
	 * Get ItemBean by item number string 
	 * @param number Item number
	 * @return ItemBean
	 *  
	 */
	public synchronized ItemBean getItem(String number) throws Exception 
	{
		number = super.sanitizeInput(number, SanitizeTypes.STRING);
		
		ItemBean ib = this.dao.retrieveItem(number); 
		if (ib == null) {
			throw new Exception(ERROR_MESSAGE_ITEM_NOT_FOUND);
		}
		
		return ib;
	}
	
	
	/**
	 * Get array from item number array
	 * 
	 * @param numbers 
	 * @return Array ItemBean
	 */
	public synchronized ItemBean[] getItems(String[] numbers) throws Exception 
	{
		ItemBean[] result = new ItemBean[numbers.length];
		
		for(int i=0; i< numbers.length; i++) 
		{
			numbers[i] = super.sanitizeInput(numbers[i], SanitizeTypes.STRING);
			result[i] = this.dao.retrieveItem(numbers[i]);
		}
		return result; 
	}
	
	
	
	/**
	 * Gets a HashMap of all CategoryBean with its Id# (unique) used as a Key
	 * in the Map. It invokes the other method (getCategories) and then 
	 * iterates over it to create a HashMap of Category's key mapped to CategoryBean.
	 * 
	 * @return A HashMap<Integer, CategoryBean> consisting of Category Id as its key
	 * 					and the CategoryBean corresponding to the Id.
	 * 
	 * @throws Exception
	 */
	public synchronized Map<Integer, CategoryBean> getCategoriesMap() throws Exception {
		List<CategoryBean> categoryList =  this.dao.retrieveCategories();
		HashMap<Integer, CategoryBean> categoryMap = new HashMap<Integer, CategoryBean>();
		
		for(CategoryBean c: categoryList) {
			categoryMap.put(c.getId(), c);
		}
		
		return categoryMap;
	}	

	/**
	 * Invokes the other method (getItems) with 0 as numberOfRecords, thus
	 * returning all the records.
	 * 
	 */
	public synchronized List<ItemBean> getSearchItems(String query) 
			throws Exception {
		
        query = this.sanitizeInput(query, SanitizeTypes.STRING);
		return this.dao.retrieveSearchItems(query);
	}        
	
	
	/**
	 * Writes the data to a JSON Cache File
	 * 
	 * @param cacheFile File path and name for the cache file
	 * @return true, on successfully writing to file, false on failure.
	 */
	public synchronized boolean exportItems(String cacheFile) throws Exception {
		
		File file = new File(cacheFile);

		if(!file.exists())
		{ 	// File does not exist, create new file and fill it
			try 
			{
				// Cache file does not exist, create it
				file.getParentFile().mkdirs(); // create dirs first
				file.createNewFile(); // now, create file 
				
				// Now get items and put them in JSON file - TODO move this to new file
				List<ItemBean> itemList = this.dao.retrieveCategoryItems(null, 0);
				
				// Now try to create JSON file
				StringBuffer jsonBuilder = new StringBuffer();
				
				for(ItemBean item : itemList) {
					jsonBuilder.append(item.toJsonString()).append(",");
			    }
				
				// remove the last ','
				jsonBuilder.deleteCharAt(jsonBuilder.length()-1);
				
				jsonBuilder.insert(0, "{\"items\":[")
						   .insert(jsonBuilder.length(), "]}");

				FileWriter fileWriterStream = new FileWriter(file, false);
				
				fileWriterStream.write(jsonBuilder.toString());				

				// Finally, close the output writer stream
				fileWriterStream.flush();
				fileWriterStream.close();
			} 
			catch (IOException ex) 
			{
				// Failed to create, let the client know
				return false;
			}
		} else {
			// file already exists as items.json!
		}
		 
		// At the end return process status
		return true;
	}
	
	/**
	 * Invokes the other method (getItems) with 0 as numberOfRecords, thus
	 * returning all the records.
	 * 
	 * @param  category      The Category Id from which the items will be returned.
	 *                        If null is provided as category, it returns ALL items
	 *                        from database, irrespective of category.
	 *                          
	 * @return List<ItemBean> A List of the Items
	 * 
	 * @throws Exception	  
	 */
	public synchronized List<ItemBean> getItems(String category) throws Exception {
		return this.getItems(category, 0);
	}

	
	/**
	 * Gets all the Items that belongs to a specific category. If null is
	 * provided as category, then it returns all items (irrespective of category)
	 * 
	 * 
	 * @see
	 * http://db.apache.org/derby/faq.html#limit 
	 * 
	 * @param  category      The Category Id from which the items will be returned.
	 *                        If null is provided as category, it returns ALL items
	 *                        from database, irrespective of category.
	 * 
	 * @param numberofRecords The number of Records to be fetched from database.                        
	 *                          
	 * @return List<ItemBean> A List of the Items
	 * 
	 * @throws Exception	  
	 */
	public synchronized List<ItemBean> getItems(String category, int numberOfRecords) 
			throws Exception {
		
		/* sanitize the input and then retrieve from the model */
		String catId = category == null ? category
					 	: this.sanitizeInput(category, SanitizeTypes.INTEGER);		
		
		List<ItemBean> ibList = this.dao.retrieveCategoryItems(catId, numberOfRecords); 
		if (ibList == null) {
			throw new Exception(ERROR_MESSAGE_CATEGORY_NOT_FOUND);
		}
		else
			return ibList;	
	}	

	
	/**
	 * Gets 3 Items from all Categories, and then puts then in a HashMap, where
	 * each Key corresponds to {@link CategoryBean}, and its value corresponds 
	 * to {@link ItemBean} in the Categories.
	 * 
	 * 
	 * @return A HashMap<CategoryBean, List<ItemBean>> consisting of Items from each Category
	 */
	public synchronized HashMap<CategoryBean, List<ItemBean>> getItemsExcerptFromCategories() 
			throws Exception {
		
		final int MAX_ITEMS_RECORD = 3;
		
		HashMap<CategoryBean, List<ItemBean>> itemList = 
							   new HashMap<CategoryBean, List<ItemBean>>();
		
		List<CategoryBean> categories = this.getCategories();
		
		// @TODO: optimize accessing of items from categories!
		for(CategoryBean c: categories) {
			List<ItemBean> list = this.getItems(c.getId() + "", MAX_ITEMS_RECORD); 
			itemList.put(c, list);
		}
		
//		System.out.println("Item Size: " + itemList.size());
		
		return itemList;
	}
		
}
