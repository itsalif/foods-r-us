
package model;

import java.text.DecimalFormat;

/**
 * A Base Model from which all the Models is extended from.
 * This provides functionalities such as Sanitizing Input,
 * which is used by other Models.
 * 
 * @author Abdullah Rubiyath
 * @author cse73194
 */
public class BaseModel {
	
	/**
	 * The Types of Sanitization
	 * INTEGER OR STRING
	 */
	protected enum SanitizeTypes {
		STRING, INTEGER
	}	
	
	
	/**
	 * Sanitizes an input to prevent it from SQL Injection. 
	 * At first removes slashes from the string (if there's any), and 
	 * then it sanitizes input. 
	 * 
	 * @param input The input to be sanitized, Input format is: [/]integer-id[/]
	 * 				Note: both slashes are optional.
	 * 
	 * @param type  Type is either STRING or INTEGER
	 * 
	 * @return The Sanitized String
	 */
	protected String sanitizeInput(String input, SanitizeTypes type) {
		
		/* replace All slashes, if there's any */
		input = input.replace("/", "");
		
		if (type == SanitizeTypes.INTEGER) 
		{
			/* Attempt to parse the input to an integer and if 
			 * no exception is thrown, then simply return the 
			 * parsed Integer as a String
			 */				
			int number = Integer.parseInt(input);
			return String.valueOf(number);			
		}
		else {
			
			/* remove anything thats not a character (a-z A-Z) 
			 * or a digit(0-9) or a space. 
			 */
			return input.replaceAll("[^A-Za-z0-9 ]", "");
			
		}		
	}
	
	
	/**
	 * Converts a digit to 2 decimal places
	 * @param number
	 * @return 
	 */
	public double formatDigits(double number) {
		DecimalFormat decim = new DecimalFormat("#.##");		
		return Double.parseDouble(decim.format(number));
	}	
		
}
