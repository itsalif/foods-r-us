package model;

/**
 * CategoryBean for creating object of type Category and getting its attributes
 * @author cse73194
 * 
 */
public class CategoryBean
{
	private int id;
	private String name;
	private String description;
	
	public CategoryBean(String description, String name, int id)
	{
		this.setId(id);
		this.setName(name);
		this.setDescription(description);
	}

	public int getId()
	{
		return id;
	}

	private void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	private void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	private void setDescription(String description)
	{
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		return this.id;
	}
	
	@Override
	public boolean equals(Object cb) 
	{
		if(cb != null && cb instanceof CategoryBean) 
		{
			// id are unique, so if id is equal they are same object
			return (this.id == ((CategoryBean) cb).id);
		}
		
		return false;
	}
}
