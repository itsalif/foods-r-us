/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Manages placing orders
 * 
 * @author Abdullah Rubiyath
 */
@XmlRootElement(name="order")
@XmlAccessorType(XmlAccessType.FIELD)

public class OrderWrapper {	
	@XmlAttribute
	private int id;
	
	@XmlAttribute
	private String submitted;

	@XmlElement
	private ClientBean customer;
	
	
	@XmlElement(name="items")
	private OrderItemWrapper itemsWrapper;
	
	@XmlElement
	private double total;
	
	@XmlElement
	private int shipping;
	
	@XmlElement(name="HST")
	private double tax;
	
	@XmlElement
	private double grandTotal;
	
	
	public OrderWrapper() {
		
	}
	
	/**
	 * 
	 * @param orderId
	 * @param orderDate
	 * @param cart
	 * @param client 
	 */
	public OrderWrapper(int orderId, String orderDate, CartBean cart, 
						ClientBean client) {
		
		/* store all the cart items in OrderWrapper's Item */
		this.itemsWrapper = new OrderItemWrapper();
		Map<ItemBean, Integer> cartItems = cart.getCartItems();
		this.id = orderId;
		this.submitted = orderDate;
		this.customer = client;
		
		this.total = cart.getTotal();
		this.tax = cart.getTax();
		this.shipping = cart.getShipping();
		this.grandTotal = cart.getGrandTotal();
		
		OrderItem newItem;
		for (ItemBean item: cartItems.keySet()) {
			
			newItem = new OrderItem(item);
			newItem.setQuantity(cartItems.get(item));
			newItem.setExtended(newItem.getQuantity() * newItem.getPrice());
			this.itemsWrapper.add(newItem);
		}
				
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the submitted
	 */
	public String getSubmitted() {
		return submitted;
	}

	/**
	 * @param submitted the submitted to set
	 */
	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}

	/**
	 * @return the items
	 */
	public OrderItemWrapper getOrderItems() {
		return this.itemsWrapper;
	}


	/**
	 * @return the customer
	 */
	public ClientBean getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(ClientBean customer) {
		this.customer = customer;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the shipping
	 */
	public int getShipping() {
		return shipping;
	}

	/**
	 * @param shipping the shipping to set
	 */
	public void setShipping(int shipping) {
		this.shipping = shipping;
	}

	/**
	 * @return the tax
	 */
	public double getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(double tax) {
		this.tax = tax;
	}

	/**
	 * @return the grandTotal
	 */
	public double getGrandTotal() {
		return grandTotal;
	}

	/**
	 * @param grandTotal the grandTotal to set
	 */
	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}	
	
}
