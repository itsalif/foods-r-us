package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * DAO to access the items database
 * @author cse73194
 * 
 */

public class ItemDAO
{
	private DataSource dataSource;
	
	/**
	 * Internal logger object to log DEBUG/INFO/WARNING/ERROR messages
	 * 
	 * @see {@link Logger} for more info
	 */
	private static final Logger logger = Logger.getLogger(ItemDAO.class.getName());

	/**
	 * 
	 * @throws Exception
	 */
	public ItemDAO() throws Exception
	{
		this.dataSource = (DataSource) (new InitialContext())
				.lookup("java:/comp/env/jdbc/EECS");

	}

	/**
	 * 
	 * @return a list containing all the available categories with their id,
	 *         name, and description
	 * @throws Exception
	 */
	public List<CategoryBean> retrieveCategories() throws Exception
	{
		Connection con = this.getConnection();
		Statement s = con.createStatement();
		this.setDatabaseSchema(s);

		String query = "SELECT ID, NAME, DESCRIPTION FROM CATEGORY";

		ResultSet r = s.executeQuery(query);
		List<CategoryBean> list = new ArrayList<CategoryBean>();
		while (r.next())
		{
			CategoryBean cb = new CategoryBean(r.getString("DESCRIPTION"),
					r.getString("NAME"), r.getInt("ID"));
			list.add(cb);
		}

		con.close();
		return list;
	}
	
	/**
	 * 
	 * @return a list containing all the available categories with their id,
	 *         name, and description
	 * 
	 * @throws Exception
	 */
	public CategoryBean retrieveCategory(String cat) throws Exception
	{
		Connection con = this.getConnection();
		Statement s = con.createStatement();
		this.setDatabaseSchema(s);
		s.setMaxRows(1);
		String query = " SELECT id, name, description " + 
					   " FROM category " +
					   " WHERE id=" + cat;

		ResultSet r = s.executeQuery(query);
		
		/* check if there is atleast 1 row */
		CategoryBean cb = null;		
		if (r.next()) {
			cb = new CategoryBean(r.getString("description"),
					r.getString("name"), r.getInt("id"));
		}
		
		con.close();
		return cb;
	}	

	/**
	 * 
	 * @param catId
	 * @param rows
	 * @return a list containing all the items of the specified category
	 * @throws Exception
	 */
	public List<ItemBean> retrieveCategoryItems(String catId, int rows)
			throws Exception
	{
		Connection con = this.getConnection();
		Statement s = con.createStatement();
		this.setDatabaseSchema(s);

		/* Only set a row if more than 0 is provided */
		if (rows > 0)
		{
			s.setMaxRows(rows);
		}

		String query;
		if (catId == null) {
			query = "Select i.number, i.name, i.price, i.qty, i.unit from ITEM i";
		}
		else {
			query = "Select i.number, i.name, i.price, i.qty, i.unit from ITEM i where i.catid = "
					+ catId;

		}
		
		ResultSet r = s.executeQuery(query);
		List<ItemBean> list = new ArrayList<ItemBean>();
		while (r.next())
		{
			ItemBean ib = new ItemBean(r.getString("NUMBER"), r.getString("NAME"),
					r.getInt("QTY"), r.getDouble("PRICE"), r.getString("UNIT"));
		
			list.add(ib);
		}

		con.close();
		return list;
	}

	/**
	 * 
	 * @param catId
	 * @return a list containing all the items of the specified category
	 * @throws Exception
	 */
	public List<ItemBean> retrieveCategoryItems(String catId) throws Exception
	{
		// 0 -> means return ALL items from Database based on catId
		return this.retrieveCategoryItems(catId, 0);

	}

	/**
	 * 
	 * @param itemNumber
	 *           A item number of the Item
	 * @return the basic info (item_number, item_name, item_price, item_quantity,
	 *         item_unit) of the specified itemNumber
	 * 
	 * @throws Exception
	 */
	public ItemBean retrieveItem(String itemNumber) throws Exception
	{
		Connection con = this.getConnection();
		Statement s = con.createStatement();
		this.setDatabaseSchema(s);

		String query = "Select i.number, i.name, i.price, i.qty, i.unit " +
					   " FROM ITEM i " +
					   " WHERE i.number = '" + itemNumber + "'";

		ResultSet r = s.executeQuery(query);
		ItemBean ib = null;
		
		// move cursor to the first row
		if (r.next()) {
			ib = new ItemBean(r.getString("NUMBER"), r.getString("NAME"),
					r.getInt("QTY"), r.getDouble("PRICE"), r.getString("UNIT"));
		}

		con.close();		
		return ib;
	}

	/**
	 * 
	 * @param searchQuery
	 * @return a list containing all the items that satisfies the search query
	 * @throws Exception
	 */
	public List<ItemBean> retrieveSearchItems(String searchQuery)
			throws Exception
	{
		Connection con = this.getConnection();
		Statement s = con.createStatement();
		this.setDatabaseSchema(s);

		String query = "SELECT i.number, i.name, i.price, i.qty, i.unit from ITEM i WHERE LOWER(i.name) LIKE '%"
				+ searchQuery.toLowerCase() + "%' OR LOWER(i.number) LIKE '%" + searchQuery.toLowerCase() + "%'";
		
		// Log query
		logger.log(Level.INFO, query);



		ResultSet r = s.executeQuery(query);
		List<ItemBean> list = new ArrayList<ItemBean>();
		while (r.next())
		{
			ItemBean ib = new ItemBean(r.getString("NUMBER"), r.getString("NAME"),
					r.getInt("QTY"), r.getDouble("PRICE"), r.getString("UNIT"));
			list.add(ib);
		}

		con.close();
		return list;
	}

	/**
	 * 
	 * @param s
	 * @throws Exception
	 */
	private void setDatabaseSchema(Statement s) throws Exception
	{
		s.executeUpdate("set schema roumani");
	}
	
	
	/**
	 * Returns the connection from datasource
	 */
	private Connection getConnection() throws Exception 
	{
		//if (false == true) {
		//	Class c   = Class.forName("org.apache.derby.jdbc.ClientDriver");
		//	return DriverManager.getConnection("jdbc:derby://indigo.cse.yorku.ca:9999/CSE",
		//			"student" , "secret");
	//	}
		return this.dataSource.getConnection();
	}
}
