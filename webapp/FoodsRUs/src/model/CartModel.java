package model;

import java.util.Map;

/**
 * Model that deals with adding/removing items to shopping cart.
 * Handles Concurrency issues on managing Cart.
 * 
 * @author cse73194
 */
public class CartModel extends BaseModel {

	/**
	 * The Amount/Price after which shipping becomes free
	 */
	public final double FREE_SHIPPING_AMOUNT = 100.0;
	
	/**
	 * The Percentage of tax, in Canada, HST is 13%
	 */
	public final double TAX_PERCENT = 13.0;
	
	/**
	 * The Flat shipping cost for orders below the 
	 * free shipping amount.
	 */
	public final int FLAT_SHIPPING_COST = 5;
	
	/* error message to not add item to cart */
	private static final String ERROR_MESSAGE_INVALID_QTY = 
			"Unable to process. Please enter a positive quantity for Item";
	
	
	/**
	 * Adds an item to cart, if the item already
	 * exists, it simply updates the item's counter by another 1
	 * 	
	 * @param cb   The CartBean which item is to be added 
	 * @param item An ItemBean to be added
	 */
	public synchronized void add(CartBean cb, ItemBean item) throws Exception {
		this.add(cb, item, "1");
	}
	

	/**
	 * Adds an {@link ItemBean} to Cart with its quantity (qty)
	 * 
	 * @param cb   The CartBean which item is to be added 
	 * @param item	An {@link ItemBean} to be added
	 * @param qty	The quantity to be added
	 * 
	 * @throws Exception 
	 */
	public synchronized void add(CartBean cb, ItemBean item, String qty) 
			throws Exception {
		Map<ItemBean, Integer> cartItems = cb.getCartItems();
		
		Integer cartQty = cartItems.get(item);
		// atleast 1 item must be added, otherwise its invalid
		int itemQty = this.validateQty(qty, 1);	
		
		int itemCount = (cartQty == null) ? itemQty : cartQty + itemQty;		
		cartItems.put(item, itemCount);
		updateTotal(cb, '+', itemQty, itemQty * item.getPrice());
	}
	
	
	/**
	 * Updates the Shopping Cart based on the {@link ItemBean} passed. This
	 * is used to handle adding/updating and or removing existing item
	 * This is a synchronized method to prevent concurrency issues.
	 * 
	 * @param cb        The CartBean which is to be updated
	 * @param item		An {@link ItemBean} corresponding to the item
	 *					being added
	 * 
	 * @param quantity The quantity of that item to be added
	 *				   on the cart.
	 */
	public synchronized void update(CartBean cb, ItemBean item, String quantity) throws Exception
	{
		int qty = this.validateQty(quantity, 0);
		Map<ItemBean, Integer> cartItems = cb.getCartItems();
		Integer cartQty = cb.getCartItems().get(item);
		
		/* Removal case: check if qty is 0 (then just remove the items) */
		if (qty == 0) {			
			/* item does exist */
			if (cartQty != null) {				
				cartItems.remove(item);
				this.updateTotal(cb, '-', cartQty, cartQty * item.getPrice());
			}			
			return;
		}
		
		// otherwise (update)
		cartItems.put(item, qty);
		if (cartQty != null) {
			/* remove from cart the old quantity and its price */
			this.updateTotal(cb, '-', cartQty, cartQty * item.getPrice());
		}
		
		/* now update with the new quantity and price */
		this.updateTotal(cb, '+', qty, qty * item.getPrice());	
	}
	
	/**
	 * Iterates through all the elements of item and invokes the 
	 * other method to update the quantity.
	 * 
	 * @param cb        The CartBean which is to be updated
	 * @param itemList	An array of ItemBeans
	 * @param quantity	An array of quantity corresponding to each ItemBean
	 * @throws Exception 
	 */
	public synchronized void update(CartBean cb, ItemBean[] itemList, 
									String[] quantity) throws Exception
	{
		/* the two lengths must be equal, if they are not, then 
		 * ignore the process.
		 */
		if (itemList.length != quantity.length) {
			return;
		}
		
		/* go through all the items in itemList */
		for (int i = 0; i < itemList.length; i++) {			
			this.update(cb, itemList[i], quantity[i]);
		}
	}	
	
	
	/**
	 * Updates the total items, total Price, tax and shipping amount, and
	 * Grand Total in CartBean. The updates are done on this separate 
	 * method to avoid concurrency issues.
	 * 
	 * @param cb        The CartBean which is to be updated
	 * @param op Either '+' or '-'
	 * @param qty The Quantity changed in the cart
	 * @param the The price changes in the cart.
	 * 
	 */
	private synchronized void updateTotal(CartBean cb, char op, int qty, double price) {
		
		assert (op == '+' || op == '-');
		
		int itemsCount = cb.getItemsCount();
		double total = cb.getTotal();
		
		if (op == '-') {
			itemsCount -= qty;
			total -= price;
		}
		else {
			itemsCount += qty;
			total += price;			
		}
		
		double tax = TAX_PERCENT * total / 100.0;
		if (total >= FREE_SHIPPING_AMOUNT) {
			cb.setShipping(0);
		}
		else {
			cb.setShipping(FLAT_SHIPPING_COST);
		}
		
		
		cb.setTax(super.formatDigits(tax));
		cb.setTotal(super.formatDigits(total));
		cb.setItemsCount(itemsCount);
		cb.setGrandTotal(super.formatDigits(tax + total + cb.getShipping()));
	}
	
	
	/**
	 * A Simple Qty validation method.
	 * 
	 * @param qty the quantity of items in string
	 * @param limit the minimum limit, below which its invalid
	 * 
	 * @return The integer (of qty) which is to be added.
	 */
	private int validateQty(String qty, int limit) throws Exception 
	{
		try 
		{
			int itemQty = Integer.parseInt(qty);
		
			// if items are 0 or less, then throw error!
			if (itemQty < limit) {
				throw new Exception(ERROR_MESSAGE_INVALID_QTY);
			}
		
			return itemQty;
		}
		catch(NumberFormatException e) {
			throw new Exception(ERROR_MESSAGE_INVALID_QTY);
		}		
	}
	

}
