package util;

import java.text.DecimalFormat;

/**
 * A Utility class that provides functionalities such as
 * converting digits to a proper format (2 Decimal places)
 * that is used across the Web App (eCommerce Site).
 * 
 * @author Abdullah Rubiyath
 */
public class ToolBox {

	
	/**
	 * Converts a digit to 2 decimal places
	 * @param number
	 * @return 
	 */
	public static double formatDigits(double number) {
		DecimalFormat decim = new DecimalFormat("#.##");		
		return Double.parseDouble(decim.format(number));
	}
	
}
