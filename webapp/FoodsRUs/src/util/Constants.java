/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * The various Constants, including Error Message, used and shared
 * across the App by both Servlet and JSPX.
 * 
 * @author cse73194
 *
 */


public class Constants {
	
	public static final String BASE_PATH = "res/";
		
	public static final String CACHE_PATH = "cache/";
	public static final String ORDER_PATH = BASE_PATH + "order/";
	public static final String CONTENT_TYPE_JSON = "text/json";
	
	public static final String KEY_CART 		  		= "cart";
	public static final String KEY_CLIENT 		  		= "client";
	public static final String KEY_TOTAL_TIME 	  		= "cart_total_time";
	public static final String KEY_TOTAL_NUM 	  		= "cart_total_num";
	public static final String KEY_CHECKOUT_ITEMS_COUNT = "checkout_count";
	
	public static final String KEY_ITEM_ADDED     = "cart_item_add";
	
	public static final String MESSAGE_ORDER_SUCCESS  = "Success! Thank you for ordering.";	
	public static final String MESSAGE_LOGIN_REQUIRED = "Please Login to continue with your order.";
	
	
	public static final String KEY_ORDER_SUCCESS = "order_success";
	public static final String KEY_ORDER_URL 	 = "order_url";
	
	public static final String EXPORT_PATH = BASE_PATH + "export/";
	public static final String FILE_ALL_ORDERS = ORDER_PATH + "all_orders.xml";
	public static final String FILE_EXT_XML = ".xml";
	public static final String FILE_EXT_JSON = ".json";

	
	public static final String KEY_ERROR_MSG = "errorMsg";
	
	public static final int ORDER_START_COUNTER = 1;
	
	public static final int ORDER_PENDING = 0;
	public static final int ORDER_COMPLETED = 1;
	
	public static final String ORDER_FILE_PREFIX = "po";
	public static final String ORDER_FILE_SEPARATOR = "_";
	
	//private static Constants instance;
	
	//private Constants() {}		
	
}
