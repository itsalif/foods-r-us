package filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import model.Item;
import model.ItemBean;

/**
 * Servlet Filter implementation class AdvertiseFilter
 * 
 * @author Abdullah Rubiyath
 * 
 */

@WebFilter("/AdvertiseFilter")
public class AdvertiseFilter implements Filter {

	private static final String itemNumber = "1409S413";
	private static final String advItemNumber = "2002H712";
	
    /**
     * Default constructor. 
     */
    public AdvertiseFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {

		 try
		    {
			  if (request.getParameter("item_number") != null && 
				  request.getParameter("item_number").equals(itemNumber)) { 
				  
				  PrintWriter out = response.getWriter();
				  
				  System.out.println ("Filtering the Request ... ITEM MATCHED");
				  
				  OutputResponseWrapper wrapper = new OutputResponseWrapper((HttpServletResponse) response);
				  
				  chain.doFilter (request, wrapper);
				  
				  
				  Item model = (Item) request.getServletContext().getAttribute("itemModel");
				  ItemBean advItem = model.getItem(advItemNumber);
				  
//				  System.out.println("--> Need to Write/Replace Response <--");
				  String responseText = wrapper.toString();	
				  // Pass the basePath to the template generator
				  String basePath = request.getServletContext().getContextPath();
				  String advertiseContent = this.itemTemplate(advItem, basePath);
				  
				  responseText = responseText
						  			.replace("<div id=\"page-sidebar-left\">", 
						  				"<div id=\"page-sidebar-left\">" + advertiseContent);
				  
				  
				  
				  response.setContentLength(responseText.toString().length());
				  out.println(responseText);
				  //System.out.println(responseText);				  	
		      
			  } else {
				  chain.doFilter(request, response);
			  }
			  
		    } 
		 	catch (IOException io) {
		      System.out.println ("IOException raised in SimpleFilter");
		    } 
		 	catch (ServletException se) {
		      System.out.println ("ServletException raised in SimpleFilter");
		    } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	
	/**
	 * An Item template which creates the ui-block for advertisement
	 * @param bean
	 * @return
	 */
	private String itemTemplate(ItemBean item, String path) {
		StringBuffer bfr = new StringBuffer();
		bfr.append("<div class=\"sidebar-block ui-vspace-margin\">") 
		   	.append("<h3>We Recommend:</h3>")
		   	.append("<div class='ui-block item-details ui-hspace'>")
		   	.append("<a href='" + path +  "/search?q=" + item.getNumber() + "'>")
		   	.append("<h4>" + item.getName() + "</h4>")
		   	.append("<h5 class='ui-number'> Item #:" + item.getNumber() + "</h5>") 
		   	.append("<h5 class='ui-price'> $" + this.formatDigits(item.getPrice())
		   			+ "<em>" + item.getUnit() + "</em></h5>")
		   	.append("</a>")
		   	.append("</div>")
			.append("</div>");
		
		return bfr.toString();
	}
	
	
	
	
	/**
	 * An inner class which wraps the response from Servlet
	 *
	 */
	private class OutputResponseWrapper extends HttpServletResponseWrapper {
		
		private final static int BUFFER_SIZE = 1024;
		
		private StringWriter sw;
		
		public OutputResponseWrapper(HttpServletResponse response) {
			super(response);
			sw = new StringWriter(BUFFER_SIZE);
		}
		
		/* Returns the Writer */
		public PrintWriter getWriter() throws IOException {
		    return new PrintWriter(sw);
		}
		
		/* Returns the String of the Response */
		public String toString() {
			return sw.toString();
		}
		
	}
	
	
	/**
	 * Converts a digit to 2 decimal places.
	 * 
	 * @param number
	 * @return 
	 */
	private double formatDigits(double number) {
		DecimalFormat decim = new DecimalFormat("#.##");		
		return Double.parseDouble(decim.format(number));
	}	
}
